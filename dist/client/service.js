'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DiscoveryClientService = undefined;

var _events = require('events');

var _events2 = _interopRequireDefault(_events);

var _dgram = require('dgram');

var _dgram2 = _interopRequireDefault(_dgram);

var _netinfo = require('sv-jscommons/netinfo');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Модуль службы клиента автопоиска клиентов
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Краткий алгоритм работы:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * 1. запускаем прослушивание udp-порта (любого, по любому интерфейсу)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * 2. каждые helloInterval секунд:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            *      - отправляем широковещательный запрос HELLO
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * 3. принимаем ответы от серверов:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            *      - генерим событие "found"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            */

const consts = require('../consts');

/**
 * @class
 * @classdesc Класс, реализующий поведение клиента автопоиска сервера
 * @extends EventEmitter
 *
 * @event DiscoveryClient#error
 * @property {Error} error Объект с информацией об ошибке
 *
 * @event DiscoveryClient#statechanged
 * @property {Error} error Объект с информацией об ошибке
 *
 * @event DiscoveryClient#found
 * @property {ServerInfo} server Объект с информацией о новом сервере
 *
 * @event DiscoveryClient#lost OBSOLETE!!!!!
 * @property {ServerInfo} server Объект с информацией о "пропавшем" сервере
 *
 * @event DiscoveryClient#close
 *
 */
class DiscoveryClientService extends _events2.default {
  /**
   * @param {Object} options {hostuid, serverPort, helloInterval, helloMissCount, type, role}
   * @param {*} logger
   */
  constructor({ hostuid, role = 'client', type = 'sportvokrug', helloInterval = 6000, checkInterfacesInterval } = {}, logger) {
    super();

    this.onSocketError = error => {
      this.log('error', 'discovery client service error:\n', error.stack || {});
      this.emit('error', error);
    };

    this.onSocketClose = () => {
      this.log('info', 'discovery client service closed');
      this.socket = null;
      this.readyState = consts.DISCOVERYCLIENT_READYSTATE_CREATED;
    };

    this.onSocketListening = () => {
      this.address = this.socket.address();
      this.readyState = consts.DISCOVERYCLIENT_READYSTATE_LISTENING;
      this.log('info', `discovery client service listening started: ${this.address.address}:${this.address.port}`);
    };

    this.onSocketMessage = (msg, rinfo) => {
      if (this.readyState === consts.DISCOVERYSERVER_READYSTATE_LISTENING) {
        setImmediate(() => this.processMessage(msg.toString(), rinfo));
      }
    };

    this.onNetworkStatusChanged = changes => {
      this.iflist = changes;
    };

    this.socket = null;
    this.logger = logger;
    this.address = null;

    this.hostuid = hostuid; // идентификатор экземпляра приложения
    this.hostname = _netinfo.NetInfo.hostname(); // имя хоста
    this.type = type; // тип приложения ( rg | la | video | ... )
    this.serverPort = null; // порт, на котором работает серверная служба автопоиска
    this.role = role; // роль экземпляра приложения
    this.helloInterval = helloInterval; // периодичность отправки запросов на поиск сервера
    this.checkInterfacesInterval = checkInterfacesInterval; // периодичность обновления списка сетевых интерфейсов

    this.helloTimer = null;

    this.iflist = []; // список доступных сетевых интервейсов

    this._readyState = consts.DISCOVERYCLIENT_READYSTATE_CREATED;
    this.log('info', '*** DiscoveryClientService instance created');
  }

  // ---------------
  get readyState() {
    return this._readyState;
  }

  set readyState(val) {
    this._readyState = val;
    this.log('verbose', `DiscoveryClientService: set readyState [${val}]`);
    this.emit('statechanged', this._readyState);
  }
  // ---------------

  /**
   * проверяет готов ли сервис к работе
   */
  ready() {
    return this.readyState === consts.DISCOVERYCLIENT_READYSTATE_LISTENING;
  }

  /**
   * Выводит отладочное сообщение если задан объект-logger
   * @param {String} level Уровень логгирования
   * @param {*} msg Текст сообщения
   */
  log(level, msg, ...args) {
    if (this.logger) {
      this.logger.log(level, msg, ...args);
    }
  }

  /**
   * Обработчик ошибки сокета
   */


  /**
   * Обработчик события закрытия сокета
   */


  /**
   * Обработчик события сокет запущен и принимает входящие сообщения
   */


  /**
   * Обработчик события "поступило сообщение"
   * @param {*} msg
   * @param {*} rinfo
   */


  /**
   * обработчик события "произошли изменения в сети"
   */


  /**
   * Запускает прослушивание сокета и прием hello-ответов от сервера
   */
  startListen() {
    var _this = this;

    return _asyncToGenerator(function* () {
      _this.log('verbose', 'DiscoveryClientService#startListen(): begin');

      if (!_this.socket) {
        try {
          _this.readyState = consts.DISCOVERYCLIENT_READYSTATE_STARTING;

          _this.socket = _dgram2.default.createSocket({ type: 'udp4', reuseAddr: true });
          _this.socket.on('error', _this.onSocketError);
          _this.socket.on('close', _this.onSocketClose);
          _this.socket.on('listening', _this.onSocketListening);
          _this.socket.on('message', _this.onSocketMessage);

          _this.log('verbose', 'DiscoveryClientService: socket created');

          // важно дождаться создания прослушивателя!!!, т.к. после этого запускаем отправку
          yield new Promise(function (resolve, reject) {
            _this.socket.bind({ exclusive: false }, function () {
              resolve(true);
            });
          });
          _this.socket.setBroadcast(true);
        } catch (err) {
          _this.readyState = consts.DISCOVERYCLIENT_READYSTATE_CREATED;
          _this.socket.close();
          _this.socket = null;
          _this.emit('error', err);
          _this.log('error', err);
        }
      }

      _this.log('verbose', 'DiscoveryClientService#startListen(): end');

      return _this.socket;
    })();
  }

  /**
   * Останавливает прослушивание сокета и прием hellо-ответов от сервера
   */
  stopListen() {
    this.log('verbose', 'DiscoveryClientService#stopListen(): begin');

    if (this.helloTimer) clearTimeout(this.helloTimer);
    if (this.socket) this.socket.close();

    this.address = null;
    this.socket = null;

    this.log('verbose', 'DiscoveryClientService#stopListen(): end');
  }

  /**
   * Запускает клиентский процесс поиска сервера
   */
  start(serverPort) {
    var _this2 = this;

    return _asyncToGenerator(function* () {
      _this2.log('verbose', 'DiscoveryClientService#start(): begin');

      if (_this2.readyState !== consts.DISCOVERYCLIENT_READYSTATE_CREATED) return;

      _this2.serverPort = serverPort;

      _this2.iflist = _netinfo.NetInfo.networkInterfaces();
      (0, _netinfo.startNetworkMonitoring)(_this2.checkInterfacesInterval, _this2.onNetworkStatusChanged);

      // важно дождаться создания прослушивателя!!!
      yield _this2.startListen();
      _this2.helloLoopTick();

      _this2.log('verbose', 'DiscoveryClientService#start(): end');
    })();
  }

  /**
   * Останавливает поиск сервера
   */
  stop() {
    this.log('verbose', 'DiscoveryClientService#stop(): begin');

    if (this.readyState !== consts.DISCOVERYSERVER_READYSTATE_LISTENING) return;

    this.readyState = consts.DISCOVERYCLIENT_READYSTATE_STOPPING;

    (0, _netinfo.stopNetworkMonitoring)(this.onNetworkStatusChanged);

    this.stopListen();

    this.readyState = consts.DISCOVERYCLIENT_READYSTATE_CREATED;

    this.log('verbose', 'DiscoveryClientService#stop(): end');
  }

  /**
   * Конструироет hello-сообщение
   */
  makeHello() {
    return {
      hostuid: this.hostuid,
      hostname: this.hostname,
      message: 'hello',
      role: this.role,
      type: this.type
    };
  }

  /**
   * Рассылает hello-сообщение по заданным интерфейсам
   * @param {Object} iflist
   */
  broadcastHelloMessage(iflist) {
    var _this3 = this;

    return _asyncToGenerator(function* () {
      _this3.log('verbose', 'DiscoveryClientService#broadcastHelloMessage(): begin');

      const helloMsg = JSON.stringify(_this3.makeHello());

      // выполняет отправку hello-сообщения по конкретному интерфейсу
      const sendHello = (() => {
        var _ref = _asyncToGenerator(function* (iface) {
          // broadcast-адрес интерфейса находится в поле iface.broadcast

          // отправляем сообщение только если флаг текущей отправки сброшен
          // это признак что предыдущая отправка прошла успешно
          if (iface.sending) {
            _this3.log('verbose', 'DiscoveryClientService#sendHello(): iface.sending == true');
            _this3.log('verbose', `DiscoveryClientService#sendHello(): ${JSON.stringify(iface)}`);
            return;
          }

          try {
            _this3.log('verbose', `DiscoveryClientService#sendHello(): sending "hello" to ${iface.broadcast}:${_this3.serverPort}`);

            yield new Promise(function (resolve, reject) {
              iface.sending = true; // флаг - идет отправка сообщения

              _this3.socket.send(helloMsg, _this3.serverPort, iface.broadcast, function (err) {
                iface.sending = false; // сбрасываем флаг отправки
                if (err) {
                  reject(err);
                } else {
                  resolve(true);
                }
              });
            });

            _this3.log('verbose', `DiscoveryClientService#sendHello(): sended "hello" to ${iface.broadcast}:${_this3.serverPort}`);
          } catch (error) {
            iface.sending = false;
            _this3.log('error', error);
          }
        });

        return function sendHello(_x) {
          return _ref.apply(this, arguments);
        };
      })();

      const tasks = [
      // сперва опрашиваем локальные интерфейсы
      ...iflist.reduce(function (memo, iface) {
        iface.status === 'up' && iface.internal && memo.push(sendHello(iface));return memo;
      }, []),
      // ... затем все остальные
      ...iflist.reduce(function (memo, iface) {
        iface.status === 'up' && !iface.internal && memo.push(sendHello(iface));return memo;
      }, [])];
      // сперва опрашиваем локальные интерфейсы

      _this3.log('verbose', 'DiscoveryClientService#broadcastHelloMessage(): end');

      return Promise.all(tasks);
    })();
  }

  /**
   * Выполняет однократно отправку hello-сообщений по всем доступным интерфейсам
   */
  helloLoopTick() {
    this.log('verbose', 'DiscoveryClientService#helloLoopTick(): begin');

    if (this.readyState === consts.DISCOVERYCLIENT_READYSTATE_LISTENING) {
      this.helloTimer = null;

      // ***** шлем hello сообщение
      this.log('verbose', 'DiscoveryClientService#helloLoopTick(): send hello broadcast');

      this.broadcastHelloMessage([...this.iflist]);

      // планируем запуск следующего цикла отправки hello сообщения
      this.helloTimer = setTimeout(() => this.helloLoopTick(), this.helloInterval);
    }

    this.log('verbose', 'DiscoveryClientService#helloLoopTick(): end');
  }

  /**
   * Обрабатывает принятое сообщение
   * @param {*} msg
   * @param {*} rinfo
   */
  processMessage(msg, rinfo) {
    let data;
    try {
      data = JSON.parse(msg);
    } catch (err) {}

    if (data && data.message === 'hello' && data.clientuid && data.clientuid === this.hostuid) {
      // это нам сообщение
      // вызываем событие 'found'
      this.emit('found', {
        hostuid: data.hostuid,
        hostname: data.hostname,
        address: rinfo.address
      });
    }
  }

  /**
   * Создает (если надо) экземпляр сервиса
   */
  static createInstance(options, logger) {
    let instance = global[consts.DISCOVERYCLIENT_GLOBAL_NAME];

    if (!instance) {
      instance = new DiscoveryClientService(options, logger);
      global[consts.DISCOVERYCLIENT_GLOBAL_NAME] = instance;
    }
    return instance;
  }

  static getInstance() {
    return global[consts.DISCOVERYCLIENT_GLOBAL_NAME];
  }
}
exports.DiscoveryClientService = DiscoveryClientService;