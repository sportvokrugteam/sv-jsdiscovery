'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.discoveryserver = undefined;

var _consts = require('../consts');

var _utils = require('sv-jscommons/utils');

/**
 * возвращает экземпляр ядра клиента discovery-службы
 *   если метод запущен в main-процессе - ищем его в global
 *   если метод запущен в renderer-процессе - ищем его в remote.global
 */
/**
 * Модуль автопоиска клиентов - серверный блок
 */

let discoveryclientCore;
const discoveryserver = exports.discoveryserver = () => {
  if (!(0, _utils.isElectron)()) {
    throw new Error('Discovery client service works only in Electron environment');
  }

  if (!discoveryclientCore) {
    discoveryclientCore = null;
    if ((0, _utils.isElectronMainProcess)()) {
      discoveryclientCore = global[_consts.DISCOVERYCLIENT_GLOBAL_NAME] || null;
    } else if ((0, _utils.isElectronRendererProcess)()) {
      discoveryclientCore = require('electron').remote.getGlobal(_consts.DISCOVERYCLIENT_GLOBAL_NAME) || null;
    }
  }

  return discoveryclientCore;
};