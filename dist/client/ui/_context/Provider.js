'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DiscoveryClientProvider = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                                   */

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _electron = require('electron');

var _lodash = require('lodash');

var _utils = require('sv-jscommons/utils');

var _netinfo = require('sv-jscommons/netinfo');

var _Context = require('./Context');

var _consts = require('../../../consts');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getLogger() {
  const logger = (0, _utils.isElectronRendererProcess)() ? _electron.remote.getGlobal('logger') : global['logger'];
  return logger || console;
}
const logger = getLogger();

function log(level, ...args) {
  logger.log(level, ...args);
}

const appconfig = (0, _utils.isElectronRendererProcess)() ? _electron.remote.getGlobal('appconf') : global['appconf'];

class DiscoveryClientProvider extends _react.Component {

  constructor(props) {
    super(props);

    this.onError = this.onError.bind(this);
    this.onStateChanged = this.onStateChanged.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onFound = this.onFound.bind(this);

    this.service = null;

    this.state = {
      readyState: null,
      servers: {},
      error: null
    };

    console.log('DiscoveryClientProvider#constructor');
  }

  onError(err) {
    console.log('DiscoveryClientProvider#onError');
    log('verbose', 'DiscoveryClientProvider: "error" event fired', err);
    console.log('DiscoveryClientProvider error', err);

    this.updateStateError(err);
  }

  onStateChanged(val) {
    console.log('DiscoveryClientProvider#onStateChanged');
    logger.log('verbose', `DiscoveryClientProvider: "statechanged" event fired [${val}]`);

    this.updateStateReadyState(val);
  }

  onClose() {
    console.log('DiscoveryClientProvider#onClose');
    logger.log('verbose', 'DiscoveryClientProvider: "close" event fired');

    this.updateStateReadyState('closed');
  }

  onFound(server) {
    // logger.log('verbose', 'DiscoveryClientProvider: "found" event fired');

    this.updateServersState(server);
  }

  updateStateError(err) {
    console.log('DiscoveryClientProvider#updateStateError');
    const error = err ? (0, _lodash.cloneDeep)(err) : err;
    this.setState(_extends({}, this.state, { error }));
  }

  updateStateReadyState(val) {
    console.log('DiscoveryClientProvider#updateStateReadyState');
    this.setState(_extends({}, this.state, { readyState: val }));
  }

  updateServersState(server) {
    if (!server) return;

    const { hostuid } = server;
    const { servers } = this.state;
    const _server = servers[server.hostuid] || {};

    let serverinfo = this.getServerInfo(server);
    if (!_server.hostuid) {
      this.setState(_extends({}, this.state, { servers: _extends({}, servers, { [hostuid]: serverinfo }) }));
    } else if (!_server.status || _server.status === 'down' || !_server.islocal && _netinfo.NetInfo.isLocalAddress(server.address)) {
      this.setState(_extends({}, this.state, { servers: _extends({}, servers, { [hostuid]: serverinfo }) }));
    }
  }

  getServerInfo(server) {
    const serverinfo = _extends({}, server);

    serverinfo.status = 'up';
    serverinfo.islocal = _netinfo.NetInfo.isLocalAddress(server.address);

    return serverinfo;
  }

  componentDidMount() {
    console.log('DiscoveryClientProvider#componentDidMount');

    // подключаемся к службе, созданной в main-процессе
    this.service = _electron.remote.getGlobal(_consts.DISCOVERYCLIENT_GLOBAL_NAME);
    if (this.service) {
      this.service.on('error', this.onError);
      this.service.on('statechanged', this.onStateChanged);
      this.service.on('found', this.onFound);
      // this.client.on('close', this.onClose);

      const readyState = this.service.readyState;
      if (readyState !== this.state.readyState) {
        this.updateStateReadyState(readyState);

        if (!this.service.ready()) {
          const { discoveryPort } = this.props;
          this.service.start(discoveryPort || appconfig.discovery.client.serverPort);
        }
      }
    }
  }

  componentWillUnmount() {
    console.log('DiscoveryClientProvider#componentWillUnmount');

    if (this.service) {
      this.service.stop();

      this.service.removeListener('error', this.onError);
      this.service.removeListener('statechanged', this.onStateChanged);
      this.service.removeListener('found', this.onFound);
      // this.client.removeListener('close', this.onClose);
    }

    this.service = null;
  }

  render() {
    console.log('DiscoveryClientProvider#render');
    const { Provider } = _Context.DiscoveryClientContext;

    return _react2.default.createElement(
      Provider,
      { value: _extends({}, this.state) },
      this.props.children
    );
  }
}
exports.DiscoveryClientProvider = DiscoveryClientProvider;
DiscoveryClientProvider.propTypes = {
  discoveryPort: _propTypes2.default.number // порт сервера службы автопоиска
};