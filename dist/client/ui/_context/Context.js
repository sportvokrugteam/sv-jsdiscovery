'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DiscoveryClientContext = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const DiscoveryClientContext = exports.DiscoveryClientContext = (0, _react.createContext)({
  readyState: null,
  error: null
}); /**
     *
     */