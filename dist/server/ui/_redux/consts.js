'use strict';

module.exports = {
  DISCOVERY_SERVER_STATECHANGED: 'discovery_server/STATECHANGED',
  DISCOVERY_SERVER_ERROR: 'discovery_server/ERROR',

  // состояние по умолчанию
  DISCOVERY_SERVER_INITIALSTATE: { readyState: '' }
};