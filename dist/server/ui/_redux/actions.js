'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                                   */

exports.statechange = statechange;
exports.error = error;

var _consts = require('./consts');

function statechange(val) {
  return {
    type: _consts.DISCOVERY_SERVER_STATECHANGED,
    payload: val
  };
}

function error(err = {}) {
  return {
    type: _consts.DISCOVERY_SERVER_ERROR,
    payload: _extends({}, err)
  };
}