'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.discoveryServerReducers = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                                   */

var _lodash = require('lodash');

var _consts = require('./consts');

function reducers(state = _consts.DISCOVERY_SERVER_INITIALSTATE, action) {
  switch (action.type) {
    case _consts.DISCOVERY_SERVER_STATECHANGED:
      return _extends({}, state, { readyState: action.payload });

    case _consts.DISCOVERY_SERVER_ERROR:
      return _extends({}, state, { error: (0, _lodash.cloneDeep)(action.payload) });

    default:
      return state;
  }
}

// объект для генерации редьюсеров
const discoveryServerReducers = exports.discoveryServerReducers = () => ({ discovery: reducers });