'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DiscoveryProvider = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                                   */

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _electron = require('electron');

var _lodash = require('lodash');

var _utils = require('sv-jscommons/utils');

var _DiscoveryContext = require('./DiscoveryContext');

var _index = require('../index');

var _consts = require('../../../consts');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getLogger() {
  const logger = (0, _utils.isElectronRendererProcess)() ? _electron.remote.getGlobal('logger') : global['logger'];
  return logger || console;
}
const logger = getLogger();

function log(level, ...args) {
  logger.log(level, ...args);
}

class DiscoveryProvider extends _react.Component {

  constructor(props) {
    super(props);

    this.onError = this.onError.bind(this);
    this.onStateChanged = this.onStateChanged.bind(this);

    this.server = null;

    this.state = {
      readyState: null,
      error: null
    };

    console.log('DiscoveryProvider#constructor');
  }

  dispatch(action) {
    const { store } = this.props;
    const dispatch = store && store.dispatch;
    dispatch && dispatch(action);
  }

  /**
   * Обрабатывает ошибки службы автопоиска
   */
  onError(err) {
    log('error', 'DiscoveryServerManager: error event fired', err);
    console.log('DiscoveryServerManager error', err);

    this.updateStateError(err);
  }

  /**
   * Обрабатываем изменение состояния слубы автопоиска
   */
  onStateChanged(val) {
    log('verbose', `DiscoveryServerManager: statechanged event fired [${val}]`);

    this.updateStateReadyState(val);
  }

  updateStateReadyState(val) {
    this.dispatch((0, _index.statechange)(val));
    this.setState(_extends({}, this.state, { readyState: val }));
  }

  updateStateError(err) {
    this.dispatch((0, _index.error)(err));
    this.setState(_extends({}, this.state, { error: err ? (0, _lodash.cloneDeep)(err) : err }));
  }

  componentDidMount() {
    console.log('DiscoveryProvider#componentDidMount');

    // подключаемся к службе автопоиска, созданной в main-процессе
    this.server = _electron.remote.getGlobal(_consts.DISCOVERYSERVER_GLOBAL_NAME);
    if (this.server) {
      this.server.on('error', this.onError);
      this.server.on('statechanged', this.onStateChanged);

      const readyState = this.server.readyState;
      if (readyState !== this.state.readyState) {
        this.updateStateReadyState(readyState);

        if (!this.server.ready()) {
          this.server.start();
        }
      }
    }
  }

  componentWillUnmount() {
    console.log('DiscoveryProvider#componentWillUnmount');

    if (this.server) {
      this.dispatch((0, _index.statechange)(''));

      this.server.stop();

      this.server.removeListener('error', this.onError);
      this.server.removeListener('statechanged', this.onStateChanged);
    }

    this.server = null;
  }

  render() {
    const { Provider } = _DiscoveryContext.DiscoveryContext;

    return _react2.default.createElement(
      Provider,
      { value: _extends({}, this.state) },
      this.props.children
    );
  }
}
exports.DiscoveryProvider = DiscoveryProvider;
DiscoveryProvider.propTypes = {
  store: _propTypes2.default.object // redux-хранилище
};