'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DiscoveryContext = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const { createContext } = _react2.default; /**
                                            *
                                            */

const DiscoveryContext = exports.DiscoveryContext = createContext({
  readyState: null,
  error: null
});