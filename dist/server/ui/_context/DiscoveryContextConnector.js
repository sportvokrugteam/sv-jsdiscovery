'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DiscoveryContextConnector = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                                   */

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _DiscoveryContext = require('./DiscoveryContext');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const { Consumer } = _DiscoveryContext.DiscoveryContext;

const DiscoveryContextConnector = exports.DiscoveryContextConnector = WrappedComponent => props => _react2.default.createElement(
  Consumer,
  null,
  discovery => _react2.default.createElement(WrappedComponent, _extends({}, props, { discovery: _extends({}, discovery) }))
);