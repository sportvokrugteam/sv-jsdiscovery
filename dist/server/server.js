'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DiscoveryServer = undefined;

var _events = require('events');

var _events2 = _interopRequireDefault(_events);

var _dgram = require('dgram');

var _dgram2 = _interopRequireDefault(_dgram);

var _os = require('os');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const consts = require('../consts');

/**
 * Events:
 *   error (error)
 *   statechanged (readyState)
 *
 * readyState:
 *   created - сервер создан, не запущен
 *   starting - стартует
 *   stopping - останавливается
 *   listening - работает
 */
/**
 * Модуль ядра сервера автопоиска клиентов
 */

class DiscoveryServer extends _events2.default {
  /**
   *
   * @param {Object} options Настройки сервера
   * @param {Logger} logger Объект для логгирования событий
   */
  constructor(options = {}, logger = null) {
    super();

    this.onSocketError = error => {
      this.log('error', 'discovery server error:\n', error.stack || {});
      this.emit('error', error);
    };

    this.onSocketClose = () => {
      this.log('info', 'discovery server closed');
      this.socket = null;
      this.readyState = consts.DISCOVERYSERVER_READYSTATE_CREATED;
    };

    this.onSocketMessage = (msg, rinfo) => {
      if (this.readyState === consts.DISCOVERYSERVER_READYSTATE_LISTENING) {
        this.processMessage(msg.toString(), rinfo);
      }
    };

    this.onSocketListening = () => {
      this.address = this.socket.address();
      this.readyState = consts.DISCOVERYSERVER_READYSTATE_LISTENING;
      this.log('info', `discovery server started: ${this.address.address}:${this.address.port}`);
    };

    this.onNetworkStatusChanged = changes => {
      if (this.readyState === consts.DISCOVERYSERVER_READYSTATE_LISTENING) {
        this.stopServer();
        this.startServer();
      }
    };

    this.socket = null;
    this.logger = logger;
    this.address = null;

    this.port = options.port || 0;
    this.hostuid = options.hostuid;
    this.hostname = (0, _os.hostname)();
    this.type = options.type || 'sportvokrug';

    this._readyState = consts.DISCOVERYSERVER_READYSTATE_CREATED;
    this.log('info', '***** DiscoveryServer instance created');
  }

  // ---------------
  get readyState() {
    return this._readyState;
  }

  set readyState(val) {
    this._readyState = val;
    // this.log('verbose', `DiscoveryServer: set readyState [${val}]`);
    this.emit('statechanged', this._readyState);
  }
  // ---------------

  /**
   * Логгирует сообщение если задан объект-logger
   * @param {*} msg Текст сообщения
   */
  log(level, msg, ...args) {
    if (this.logger) {
      this.logger.log(level, msg, ...args);
    }
  }

  /**
   * проверяет готов ли сервер к работе
   */
  ready() {
    return this.readyState === consts.DISCOVERYSERVER_READYSTATE_LISTENING;
  }

  /**
   * Обработчик ошибок сокета
   */


  /**
   * Обработчик события закрытия сокета
   */


  /**
   * Обработчик события "поступило сообщение"
   * @param {*} msg
   * @param {*} rinfo
   */


  /**
   * Обработчик события сокет запущен и принимает входящие сообщения
   */


  /**
   * Обработчик события изменения списка сетевых интерфесов
   */


  /**
   * Запускает прослушивание на udp-порту
   */
  startServer() {
    this.log('verbose', 'DiscoveryServer#startServer(): begin');

    if (!this.socket) {
      try {
        this.readyState = consts.DISCOVERYSERVER_READYSTATE_STARTING;

        this.socket = _dgram2.default.createSocket({ type: 'udp4', reuseAddr: true });
        this.socket.on('error', this.onSocketError);
        this.socket.on('listening', this.onSocketListening);
        this.socket.on('message', this.onSocketMessage);
        this.socket.on('close', this.onSocketClose);

        this.log('verbose', 'socket created');

        this.socket.bind({ port: this.port, exclusive: false });

        this.log('verbose', 'socket binded');
      } catch (error) {
        this.readyState = consts.DISCOVERYSERVER_READYSTATE_CREATED;
        this.socket.close();
        this.socket = null;
        this.emit('error', error);
        this.log('error', error);
      }
    }

    this.log('verbose', 'DiscoveryServer#startServer(): end');

    return this.socket;
  }

  /**
   * Останавливает прослушивание на udp-порту
   */
  stopServer() {
    this.log('verbose', 'DiscoveryServer#stopServer(): begin');

    this.readyState = consts.DISCOVERYSERVER_READYSTATE_STOPPING;
    if (this.socket) {
      this.socket.close();
    }
    this.address = null;
    this.socket = null;

    this.log('verbose', 'DiscoveryServer#stopServer(): end');
  }

  /**
   * Запускает сервер автопоиска
   */
  start() {
    this.log('verbose', `DiscoveryServer#start(): begin`);

    this.startServer();

    this.log('verbose', `DiscoveryServer#start(): end`);
  }

  /**
   * Останавливает сервер автопоиска
   */
  stop() {
    this.log('verbose', `DiscoveryServer#stop(): begin`);

    this.stopServer();

    this.log('verbose', `DiscoveryServer#stop(): end`);
  }

  /**
   *
   * @param {*} msg
   * @param {*} rinfo
   */
  processMessage(msg, rinfo) {
    if (this.readyState === consts.DISCOVERYSERVER_READYSTATE_LISTENING) {
      let inMsg;
      try {
        inMsg = JSON.parse(msg);
      } catch (err) {}

      if (inMsg && inMsg.message === 'hello') {
        const outMsg = {
          hostuid: this.hostuid,
          hostname: this.hostname,
          message: 'hello',
          clientuid: inMsg.hostuid || ''
        };
        this.socket.send(JSON.stringify(outMsg), rinfo.port, rinfo.address, err => {
          if (err) {
            this.log('error', err);
          }
        });
      }
    }
  }

  /**
   * Создает (если надо) экземпляр сервиса
   */
  static createInstance(options, logger) {
    let instance = global[consts.DISCOVERYSERVER_GLOBAL_NAME];

    if (!instance) {
      instance = new DiscoveryServer(options, logger);
      global[consts.DISCOVERYSERVER_GLOBAL_NAME] = instance;
    }
    return instance;
  }

  static getInstance() {
    return global[consts.DISCOVERYSERVER_GLOBAL_NAME];
  }
}exports.DiscoveryServer = DiscoveryServer; // class DiscoveryServer ...