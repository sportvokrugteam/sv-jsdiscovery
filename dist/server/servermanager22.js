'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getDiscoveryServerManager = exports.createDiscoveryServerManager = exports.DiscoveryServerManager = undefined;

var _electron = require('electron');

var _utils = require('sv-jscommons/utils');

var _consts = require('../consts');

var _actions = require('./actions');

/**
 * Класс управления сервером автопоиска и хранилищем состояний
 */

function getLogger() {
  const logger = (0, _utils.isElectronRendererProcess)() ? _electron.remote.getGlobal('logger') : global['logger'];
  return logger || console;
}
const logger = getLogger();

function log(level, ...args) {
  logger.log(level, ...args);
}

/**
 * управление сервером автопоиска
 */
class DiscoveryServerManager {
  constructor(dispatch) {
    this.onError = err => {
      logger.log('verbose', 'DiscoveryServerManager: error event fired');
      console.log('DiscoveryServerManager error', err);
      this.dispatch((0, _actions.error)(err));
    };

    this.onStateChanged = val => {
      logger.log('verbose', `DiscoveryServerManager: statechanged event fired [${val}]`);
      this.dispatch((0, _actions.statechange)(val));
    };

    if (!(0, _utils.isElectronRendererProcess)()) {
      throw new Error('Class DiscoveryServerManager should be created only in renderer process');
    }

    this.dispatch = dispatch;

    this.server = _electron.remote.getGlobal(_consts.DISCOVERYSERVER_GLOBAL_NAME);
    this.server.on('error', this.onError);
    this.server.on('statechanged', this.onStateChanged);

    // обновляем стейт сервера автопоиска
    this.dispatch && this.dispatch((0, _actions.statechange)(this.server.readyState));
    log('info', '***** DiscoveryServer instance created');
  }

}

exports.DiscoveryServerManager = DiscoveryServerManager; /**
                                                          * создает сервер для webContext
                                                          */

let discoveryServerManager;
const createDiscoveryServerManager = exports.createDiscoveryServerManager = store => {
  if (!(0, _utils.isElectronRendererProcess)()) {
    throw new Error('Can create discovery server manager (webContext) only in renderer process');
  }
  if (!discoveryServerManager) {
    discoveryServerManager = new DiscoveryServerManager(store && store.dispatch);
  }
  return discoveryServerManager;
};

/**
 * возвращает сервер для webContext
 */
const getDiscoveryServerManager = exports.getDiscoveryServerManager = () => {
  if (!(0, _utils.isElectronRendererProcess)()) {
    throw new Error('Method getDiscoveryServerManager could be called only in renderer process');
  }
  if (!discoveryServerManager) {
    throw new Error('There is no DiscoveryServerManager instance. Call createDiscoveryServerManager first.');
  }
  return discoveryServerManager;
};