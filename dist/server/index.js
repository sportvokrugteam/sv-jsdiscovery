'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.discoveryserver = undefined;

var _consts = require('../consts');

var _utils = require('sv-jscommons/utils');

// export { discoveryServerReducers } from './reducers';
// export { createDiscoveryServerManager } from './serverManager';

/**
 * возвращает экземпляр ядра discovery-сервера
 *   если метод запущен в main-процессе - ищем его в global
 *   если метод запущен в renderer-процессе - ищем его в remote.global
 */
/**
 * Модуль автопоиска клиентов - серверный блок
 */

let discoveryserverCore;
const discoveryserver = exports.discoveryserver = () => {
  if (!(0, _utils.isElectron)()) {
    throw new Error('Discovery server module works only in Electron environment');
  }

  if (!discoveryserverCore) {
    discoveryserverCore = null;
    if ((0, _utils.isElectronMainProcess)()) {
      discoveryserverCore = global[_consts.DISCOVERYSERVER_GLOBAL_NAME] || null;
    } else if ((0, _utils.isElectronRendererProcess)()) {
      discoveryserverCore = require('electron').remote.getGlobal(_consts.DISCOVERYSERVER_GLOBAL_NAME) || null;
    }
  }

  return discoveryserverCore;
};