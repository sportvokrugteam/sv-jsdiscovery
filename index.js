/**
 *
 */

module.exports.DiscoveryServer = require('./dist/server/server').DiscoveryServer;

module.exports.DiscoveryServerContext = require('./dist/server/ui').DiscoveryContext;
module.exports.DiscoveryServerProvider = require('./dist/server/ui').DiscoveryProvider;
module.exports.DiscoveryServerContextConnector = require('./dist/server/ui').DiscoveryContextConnector;

module.exports.discoveryServerReducers = require('./dist/server/ui').discoveryServerReducers;

module.exports.DiscoveryClient = {
  Service: require('./dist/client/service').DiscoveryClientService,
  Provider: require('./dist/client/ui/_context/Provider').DiscoveryClientProvider,
  ContextConnector: require('./dist/client/ui/_context/Connector').DiscoveryClientContextConnector,
};
