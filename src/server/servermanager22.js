/**
 * Класс управления сервером автопоиска и хранилищем состояний
 */

import { remote } from 'electron';
import { isElectronRendererProcess } from 'sv-jscommons/utils';

import { DISCOVERYSERVER_GLOBAL_NAME } from '../consts';
import { statechange, error } from './actions';

function getLogger() {
  const logger = isElectronRendererProcess() ? remote.getGlobal('logger') : global['logger'];
  return logger || console;
}
const logger = getLogger();

function log(level, ...args) {
  logger.log(level, ...args);
}

/**
 * управление сервером автопоиска
 */
export class DiscoveryServerManager {
  constructor(dispatch) {
    if (!isElectronRendererProcess()) {
      throw new Error('Class DiscoveryServerManager should be created only in renderer process');
    }

    this.dispatch = dispatch;

    this.server = remote.getGlobal(DISCOVERYSERVER_GLOBAL_NAME);
    this.server.on('error', this.onError);
    this.server.on('statechanged', this.onStateChanged);

    // обновляем стейт сервера автопоиска
    this.dispatch && this.dispatch(statechange(this.server.readyState));
    log('info', '***** DiscoveryServer instance created');
  }

  onError = (err) => {
    logger.log('verbose', 'DiscoveryServerManager: error event fired');
    console.log('DiscoveryServerManager error', err);
    this.dispatch(error(err));
  }

  onStateChanged = (val) => {
    logger.log('verbose', `DiscoveryServerManager: statechanged event fired [${val}]`);
    this.dispatch(statechange(val));
  }
}

/**
 * создает сервер для webContext
 */
let discoveryServerManager;
export const createDiscoveryServerManager = (store) => {
  if (!isElectronRendererProcess()) {
    throw new Error('Can create discovery server manager (webContext) only in renderer process');
  }
  if (!discoveryServerManager) {
    discoveryServerManager = new DiscoveryServerManager(store && store.dispatch);
  }
  return discoveryServerManager;
};

/**
 * возвращает сервер для webContext
 */
export const getDiscoveryServerManager = () => {
  if (!isElectronRendererProcess()) {
    throw new Error('Method getDiscoveryServerManager could be called only in renderer process');
  }
  if (!discoveryServerManager) {
    throw new Error('There is no DiscoveryServerManager instance. Call createDiscoveryServerManager first.');
  }
  return discoveryServerManager;
};
