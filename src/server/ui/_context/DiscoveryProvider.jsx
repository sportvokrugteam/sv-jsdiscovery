/**
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { remote } from 'electron';
import { cloneDeep } from 'lodash';

import { isElectronRendererProcess } from 'sv-jscommons/utils';

import { DiscoveryContext } from './DiscoveryContext';

import { statechange, error } from '../index';

import { DISCOVERYSERVER_GLOBAL_NAME } from '../../../consts';

function getLogger() {
  const logger = isElectronRendererProcess() ? remote.getGlobal('logger') : global['logger'];
  return logger || console;
}
const logger = getLogger();

function log(level, ...args) {
  logger.log(level, ...args);
}

export class DiscoveryProvider extends Component {
  static propTypes = {
    store: PropTypes.object, // redux-хранилище
  }

  constructor(props) {
    super(props);

    this.onError = this.onError.bind(this);
    this.onStateChanged = this.onStateChanged.bind(this);

    this.server = null;

    this.state = {
      readyState: null,
      error: null,
    }

    console.log('DiscoveryProvider#constructor');
  }

  dispatch(action) {
    const { store } = this.props;
    const dispatch = store && store.dispatch;
    dispatch && dispatch(action);
  }

  /**
   * Обрабатывает ошибки службы автопоиска
   */
  onError(err) {
    log('error', 'DiscoveryServerManager: error event fired', err);
    console.log('DiscoveryServerManager error', err);

    this.updateStateError(err);
  }

  /**
   * Обрабатываем изменение состояния слубы автопоиска
   */
  onStateChanged(val) {
    log('verbose', `DiscoveryServerManager: statechanged event fired [${val}]`);

    this.updateStateReadyState(val);
  }

  updateStateReadyState(val) {
    this.dispatch(statechange(val));
    this.setState({ ...this.state, readyState: val });
  }

  updateStateError(err) {
    this.dispatch(error(err));
    this.setState({ ...this.state, error: err ? cloneDeep(err) : err });
  }

  componentDidMount() {
    console.log('DiscoveryProvider#componentDidMount');

    // подключаемся к службе автопоиска, созданной в main-процессе
    this.server = remote.getGlobal(DISCOVERYSERVER_GLOBAL_NAME);
    if (this.server) {
      this.server.on('error', this.onError);
      this.server.on('statechanged', this.onStateChanged);

      const readyState = this.server.readyState;
      if (readyState !== this.state.readyState) {
        this.updateStateReadyState(readyState);

        if (!this.server.ready()) {
          this.server.start();
        }
      }
    }
  }

  componentWillUnmount() {
    console.log('DiscoveryProvider#componentWillUnmount');

    if (this.server) {
      this.dispatch(statechange(''));

      this.server.stop();

      this.server.removeListener('error', this.onError);
      this.server.removeListener('statechanged', this.onStateChanged);
    }

    this.server = null;
  }

  render() {
    const { Provider } = DiscoveryContext;

    return (
      <Provider value={{ ...this.state }}>
        {this.props.children}
      </Provider>
    );
  }
}
