/**
 *
 */

import React from 'react';

const { createContext } = React;

export const DiscoveryContext = createContext({
  readyState: null,
  error: null,
});
