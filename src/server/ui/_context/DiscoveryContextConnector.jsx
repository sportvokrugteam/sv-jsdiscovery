/**
 *
 */

import React from 'react';
import { DiscoveryContext } from './DiscoveryContext';

const { Consumer } = DiscoveryContext;

export const DiscoveryContextConnector =
  WrappedComponent =>
    props => (
      <Consumer>
        {discovery => <WrappedComponent {...props} discovery={{ ...discovery }} />}
      </Consumer>
    );
