/**
 *
 */

import { cloneDeep } from 'lodash';
import {
  DISCOVERY_SERVER_STATECHANGED,
  DISCOVERY_SERVER_ERROR,
  DISCOVERY_SERVER_INITIALSTATE
} from './consts';

function reducers(state = DISCOVERY_SERVER_INITIALSTATE, action) {
  switch (action.type) {
    case DISCOVERY_SERVER_STATECHANGED:
      return { ...state, readyState: action.payload };

    case DISCOVERY_SERVER_ERROR:
      return { ...state, error: cloneDeep(action.payload) };

    default:
      return state;
  }
}

// объект для генерации редьюсеров
export const discoveryServerReducers = () =>
  ({ discovery: reducers });
