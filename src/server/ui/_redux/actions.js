/**
 *
 */

import {
  DISCOVERY_SERVER_STATECHANGED,
  DISCOVERY_SERVER_ERROR
} from './consts';

export function statechange(val) {
  return {
    type: DISCOVERY_SERVER_STATECHANGED,
    payload: val,
  };
}

export function error(err = {}) {
  return {
    type: DISCOVERY_SERVER_ERROR,
    payload: { ...err },
  };
}
