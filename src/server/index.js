/**
 * Модуль автопоиска клиентов - серверный блок
 */

import { DISCOVERYSERVER_GLOBAL_NAME } from '../consts';
import {
  isElectron,
  isElectronRendererProcess,
  isElectronMainProcess
} from 'sv-jscommons/utils';

// export { discoveryServerReducers } from './reducers';
// export { createDiscoveryServerManager } from './serverManager';

/**
 * возвращает экземпляр ядра discovery-сервера
 *   если метод запущен в main-процессе - ищем его в global
 *   если метод запущен в renderer-процессе - ищем его в remote.global
 */
let discoveryserverCore;
export const discoveryserver = () => {
  if (!isElectron()) {
    throw new Error('Discovery server module works only in Electron environment');
  }

  if (!discoveryserverCore) {
    discoveryserverCore = null;
    if (isElectronMainProcess()) {
      discoveryserverCore = global[DISCOVERYSERVER_GLOBAL_NAME] || null;
    }
    else if (isElectronRendererProcess()) {
      discoveryserverCore = require('electron').remote.getGlobal(DISCOVERYSERVER_GLOBAL_NAME) || null;
    }
  }

  return discoveryserverCore;
};
