/**
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { remote } from 'electron';
import { cloneDeep, isEqual } from 'lodash';

import { isElectronRendererProcess } from 'sv-jscommons/utils';
import { NetInfo } from 'sv-jscommons/netinfo';

import { DiscoveryClientContext } from './Context';

import { DISCOVERYCLIENT_GLOBAL_NAME } from '../../../consts';

function getLogger() {
  const logger = isElectronRendererProcess() ? remote.getGlobal('logger') : global['logger'];
  return logger || console;
}
const logger = getLogger();

function log(level, ...args) {
  logger.log(level, ...args);
}

const appconfig = isElectronRendererProcess() ? remote.getGlobal('appconf') : global['appconf'];

export class DiscoveryClientProvider extends Component {
  static propTypes = {
    discoveryPort: PropTypes.number, // порт сервера службы автопоиска
  }

  constructor(props) {
    super(props);

    this.onError = this.onError.bind(this);
    this.onStateChanged = this.onStateChanged.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onFound = this.onFound.bind(this);

    this.service = null;

    this.state = {
      readyState: null,
      servers: {},
      error: null,
    }

    console.log('DiscoveryClientProvider#constructor');
  }

  onError(err) {
    console.log('DiscoveryClientProvider#onError');
    log('verbose', 'DiscoveryClientProvider: "error" event fired', err);
    console.log('DiscoveryClientProvider error', err);

    this.updateStateError(err);
  }

  onStateChanged(val) {
    console.log('DiscoveryClientProvider#onStateChanged');
    logger.log('verbose', `DiscoveryClientProvider: "statechanged" event fired [${val}]`);

    this.updateStateReadyState(val);
  }

  onClose() {
    console.log('DiscoveryClientProvider#onClose');
    logger.log('verbose', 'DiscoveryClientProvider: "close" event fired');

    this.updateStateReadyState('closed');
  }

  onFound(server) {
    // logger.log('verbose', 'DiscoveryClientProvider: "found" event fired');

    this.updateServersState(server);
  }

  updateStateError(err) {
    console.log('DiscoveryClientProvider#updateStateError');
    const error = err ? cloneDeep(err) : err;
    this.setState({ ...this.state, error });
  }

  updateStateReadyState(val) {
    console.log('DiscoveryClientProvider#updateStateReadyState');
    this.setState({ ...this.state, readyState: val });
  }

  updateServersState(server) {
    if (!server) return;

    const { hostuid } = server;
    const { servers } = this.state;
    const _server = (servers[server.hostuid] || {});

    let serverinfo = this.getServerInfo(server);
    if (!_server.hostuid) {
      this.setState({ ...this.state, servers: { ...servers, [hostuid]: serverinfo } });
    }
    else if (!_server.status || _server.status === 'down' || (!_server.islocal && NetInfo.isLocalAddress(server.address))) {
      this.setState({ ...this.state, servers: { ...servers, [hostuid]: serverinfo } });
    }
  }

  getServerInfo(server) {
    const serverinfo = { ...server };

    serverinfo.status = 'up';
    serverinfo.islocal = NetInfo.isLocalAddress(server.address);

    return serverinfo;
  }

  componentDidMount() {
    console.log('DiscoveryClientProvider#componentDidMount');

    // подключаемся к службе, созданной в main-процессе
    this.service = remote.getGlobal(DISCOVERYCLIENT_GLOBAL_NAME);
    if (this.service) {
      this.service.on('error', this.onError);
      this.service.on('statechanged', this.onStateChanged);
      this.service.on('found', this.onFound);
      // this.client.on('close', this.onClose);

      const readyState = this.service.readyState;
      if (readyState !== this.state.readyState) {
        this.updateStateReadyState(readyState);

        if (!this.service.ready()) {
          const { discoveryPort } = this.props;
          this.service.start(discoveryPort || appconfig.discovery.client.serverPort);
        }
      }
    }
  }

  componentWillUnmount() {
    console.log('DiscoveryClientProvider#componentWillUnmount');

    if (this.service) {
      this.service.stop();

      this.service.removeListener('error', this.onError);
      this.service.removeListener('statechanged', this.onStateChanged);
      this.service.removeListener('found', this.onFound);
      // this.client.removeListener('close', this.onClose);
    }

    this.service = null;
  }

  render() {
    console.log('DiscoveryClientProvider#render');
    const { Provider } = DiscoveryClientContext;

    return (
      <Provider value={{ ...this.state }}>
        {this.props.children}
      </Provider>
    );
  }
}
