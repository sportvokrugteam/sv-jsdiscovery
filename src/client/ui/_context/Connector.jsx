/**
 *
 */

import React from 'react';
import { DiscoveryClientContext } from './Context';

const { Consumer } = DiscoveryClientContext;

export const DiscoveryClientContextConnector =
  WrappedComponent =>
    props => (
      <Consumer>
        {discovery => <WrappedComponent {...props} discovery={{ ...discovery }} />}
      </Consumer>
    );
