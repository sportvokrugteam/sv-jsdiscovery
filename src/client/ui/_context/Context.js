/**
 *
 */

import React, { createContext } from 'react';

export const DiscoveryClientContext = createContext({
  readyState: null,
  error: null,
});
