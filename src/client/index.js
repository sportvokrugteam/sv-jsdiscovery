/**
 * Модуль автопоиска клиентов - серверный блок
 */

import { DISCOVERYCLIENT_GLOBAL_NAME } from '../consts';
import {
  isElectron,
  isElectronRendererProcess,
  isElectronMainProcess
} from 'sv-jscommons/utils';

/**
 * возвращает экземпляр ядра клиента discovery-службы
 *   если метод запущен в main-процессе - ищем его в global
 *   если метод запущен в renderer-процессе - ищем его в remote.global
 */
let discoveryclientCore;
export const discoveryserver = () => {
  if (!isElectron()) {
    throw new Error('Discovery client service works only in Electron environment');
  }

  if (!discoveryclientCore) {
    discoveryclientCore = null;
    if (isElectronMainProcess()) {
      discoveryclientCore = global[DISCOVERYCLIENT_GLOBAL_NAME] || null;
    }
    else if (isElectronRendererProcess()) {
      discoveryclientCore = require('electron').remote.getGlobal(DISCOVERYCLIENT_GLOBAL_NAME) || null;
    }
  }

  return discoveryclientCore;
};
