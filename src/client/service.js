/**
 * Модуль службы клиента автопоиска клиентов
 *
 * Краткий алгоритм работы:
 * 1. запускаем прослушивание udp-порта (любого, по любому интерфейсу)
 * 2. каждые helloInterval секунд:
 *      - отправляем широковещательный запрос HELLO
 * 3. принимаем ответы от серверов:
 *      - генерим событие "found"
 */

import EventEmitter from 'events';
import dgram from 'dgram';
import {
  NetInfo,
  startNetworkMonitoring,
  stopNetworkMonitoring
} from 'sv-jscommons/netinfo';

const consts = require('../consts');

/**
 * @class
 * @classdesc Класс, реализующий поведение клиента автопоиска сервера
 * @extends EventEmitter
 *
 * @event DiscoveryClient#error
 * @property {Error} error Объект с информацией об ошибке
 *
 * @event DiscoveryClient#statechanged
 * @property {Error} error Объект с информацией об ошибке
 *
 * @event DiscoveryClient#found
 * @property {ServerInfo} server Объект с информацией о новом сервере
 *
 * @event DiscoveryClient#lost OBSOLETE!!!!!
 * @property {ServerInfo} server Объект с информацией о "пропавшем" сервере
 *
 * @event DiscoveryClient#close
 *
 */
export class DiscoveryClientService extends EventEmitter {
  /**
   * @param {Object} options {hostuid, serverPort, helloInterval, helloMissCount, type, role}
   * @param {*} logger
   */
  constructor({ hostuid, role = 'client', type = 'sportvokrug', helloInterval = 6000, checkInterfacesInterval } = {}, logger) {
    super();
    this.socket = null;
    this.logger = logger;
    this.address = null;

    this.hostuid = hostuid; // идентификатор экземпляра приложения
    this.hostname = NetInfo.hostname(); // имя хоста
    this.type = type; // тип приложения ( rg | la | video | ... )
    this.serverPort = null; // порт, на котором работает серверная служба автопоиска
    this.role = role; // роль экземпляра приложения
    this.helloInterval = helloInterval; // периодичность отправки запросов на поиск сервера
    this.checkInterfacesInterval = checkInterfacesInterval; // периодичность обновления списка сетевых интерфейсов

    this.helloTimer = null;

    this.iflist = []; // список доступных сетевых интервейсов

    this._readyState = consts.DISCOVERYCLIENT_READYSTATE_CREATED;
    this.log('info', '*** DiscoveryClientService instance created');
  }

  // ---------------
  get readyState() {
    return this._readyState;
  }

  set readyState(val) {
    this._readyState = val;
    this.log('verbose', `DiscoveryClientService: set readyState [${val}]`);
    this.emit('statechanged', this._readyState);
  }
  // ---------------

  /**
   * проверяет готов ли сервис к работе
   */
  ready() {
    return this.readyState === consts.DISCOVERYCLIENT_READYSTATE_LISTENING;
  }

  /**
   * Выводит отладочное сообщение если задан объект-logger
   * @param {String} level Уровень логгирования
   * @param {*} msg Текст сообщения
   */
  log(level, msg, ...args) {
    if (this.logger) {
      this.logger.log(level, msg, ...args);
    }
  }

  /**
   * Обработчик ошибки сокета
   */
  onSocketError = (error) => {
    this.log('error', 'discovery client service error:\n', error.stack || {});
    this.emit('error', error);
  }

  /**
   * Обработчик события закрытия сокета
   */
  onSocketClose = () => {
    this.log('info', 'discovery client service closed');
    this.socket = null
    this.readyState = consts.DISCOVERYCLIENT_READYSTATE_CREATED;
  }

  /**
   * Обработчик события сокет запущен и принимает входящие сообщения
   */
  onSocketListening = () => {
    this.address = this.socket.address();
    this.readyState = consts.DISCOVERYCLIENT_READYSTATE_LISTENING;
    this.log('info', `discovery client service listening started: ${this.address.address}:${this.address.port}`);
  }

  /**
   * Обработчик события "поступило сообщение"
   * @param {*} msg
   * @param {*} rinfo
   */
  onSocketMessage = (msg, rinfo) => {
    if (this.readyState === consts.DISCOVERYSERVER_READYSTATE_LISTENING) {
      setImmediate(() => this.processMessage(msg.toString(), rinfo));
    }
  }

  /**
   * обработчик события "произошли изменения в сети"
   */
  onNetworkStatusChanged = changes => {
    this.iflist = changes;
  }

  /**
   * Запускает прослушивание сокета и прием hello-ответов от сервера
   */
  async startListen() {
    this.log('verbose', 'DiscoveryClientService#startListen(): begin');

    if (!this.socket) {
      try {
        this.readyState = consts.DISCOVERYCLIENT_READYSTATE_STARTING;

        this.socket = dgram.createSocket({ type: 'udp4', reuseAddr: true });
        this.socket.on('error', this.onSocketError);
        this.socket.on('close', this.onSocketClose);
        this.socket.on('listening', this.onSocketListening);
        this.socket.on('message', this.onSocketMessage);

        this.log('verbose', 'DiscoveryClientService: socket created');

        // важно дождаться создания прослушивателя!!!, т.к. после этого запускаем отправку
        await (new Promise((resolve, reject) => {
          this.socket.bind({ exclusive: false }, () => {
            resolve(true);
          });
        }));
        this.socket.setBroadcast(true);
      }
      catch (err) {
        this.readyState = consts.DISCOVERYCLIENT_READYSTATE_CREATED;
        this.socket.close();
        this.socket = null;
        this.emit('error', err);
        this.log('error', err);
      }
    }

    this.log('verbose', 'DiscoveryClientService#startListen(): end');

    return this.socket;
  }

  /**
   * Останавливает прослушивание сокета и прием hellо-ответов от сервера
   */
  stopListen() {
    this.log('verbose', 'DiscoveryClientService#stopListen(): begin');

    if (this.helloTimer) clearTimeout(this.helloTimer);
    if (this.socket) this.socket.close();

    this.address = null;
    this.socket = null;

    this.log('verbose', 'DiscoveryClientService#stopListen(): end');
  }

  /**
   * Запускает клиентский процесс поиска сервера
   */
  async start(serverPort) {
    this.log('verbose', 'DiscoveryClientService#start(): begin');

    if (this.readyState !== consts.DISCOVERYCLIENT_READYSTATE_CREATED) return;

    this.serverPort = serverPort;

    this.iflist = NetInfo.networkInterfaces();
    startNetworkMonitoring(this.checkInterfacesInterval, this.onNetworkStatusChanged);

    // важно дождаться создания прослушивателя!!!
    await this.startListen();
    this.helloLoopTick();

    this.log('verbose', 'DiscoveryClientService#start(): end');
  }

  /**
   * Останавливает поиск сервера
   */
  stop() {
    this.log('verbose', 'DiscoveryClientService#stop(): begin');

    if (this.readyState !== consts.DISCOVERYSERVER_READYSTATE_LISTENING) return;

    this.readyState = consts.DISCOVERYCLIENT_READYSTATE_STOPPING;

    stopNetworkMonitoring(this.onNetworkStatusChanged);

    this.stopListen();

    this.readyState = consts.DISCOVERYCLIENT_READYSTATE_CREATED;

    this.log('verbose', 'DiscoveryClientService#stop(): end');
  }

  /**
   * Конструироет hello-сообщение
   */
  makeHello() {
    return {
      hostuid: this.hostuid,
      hostname: this.hostname,
      message: 'hello',
      role: this.role,
      type: this.type,
    };
  }

  /**
   * Рассылает hello-сообщение по заданным интерфейсам
   * @param {Object} iflist
   */
  async broadcastHelloMessage(iflist) {
    this.log('verbose', 'DiscoveryClientService#broadcastHelloMessage(): begin');

    const helloMsg = JSON.stringify(this.makeHello());

    // выполняет отправку hello-сообщения по конкретному интерфейсу
    const sendHello = async (iface) => {
      // broadcast-адрес интерфейса находится в поле iface.broadcast

      // отправляем сообщение только если флаг текущей отправки сброшен
      // это признак что предыдущая отправка прошла успешно
      if (iface.sending) {
        this.log('verbose', 'DiscoveryClientService#sendHello(): iface.sending == true');
        this.log('verbose', `DiscoveryClientService#sendHello(): ${JSON.stringify(iface)}`);
        return;
      }

      try {
        this.log('verbose', `DiscoveryClientService#sendHello(): sending "hello" to ${iface.broadcast}:${this.serverPort}`);

        await (new Promise((resolve, reject) => {
          iface.sending = true; // флаг - идет отправка сообщения

          this.socket.send(helloMsg, this.serverPort, iface.broadcast, (err) => {
            iface.sending = false; // сбрасываем флаг отправки
            if (err) {
              reject(err);
            }
            else {
              resolve(true);
            }
          });
        }));

        this.log('verbose', `DiscoveryClientService#sendHello(): sended "hello" to ${iface.broadcast}:${this.serverPort}`);
      }
      catch (error) {
        iface.sending = false;
        this.log('error', error);
      }
    };

    const tasks = [
      // сперва опрашиваем локальные интерфейсы
      ...iflist.reduce((memo, iface) => { iface.status === 'up' && iface.internal && memo.push(sendHello(iface)); return memo; }, []),
      // ... затем все остальные
      ...iflist.reduce((memo, iface) => { iface.status === 'up' && !iface.internal && memo.push(sendHello(iface)); return memo; }, []),
    ];
    // сперва опрашиваем локальные интерфейсы

    this.log('verbose', 'DiscoveryClientService#broadcastHelloMessage(): end');

    return Promise.all(tasks);
  }

  /**
   * Выполняет однократно отправку hello-сообщений по всем доступным интерфейсам
   */
  helloLoopTick() {
    this.log('verbose', 'DiscoveryClientService#helloLoopTick(): begin');

    if (this.readyState === consts.DISCOVERYCLIENT_READYSTATE_LISTENING) {
      this.helloTimer = null;

      // ***** шлем hello сообщение
      this.log('verbose', 'DiscoveryClientService#helloLoopTick(): send hello broadcast');

      this.broadcastHelloMessage([...this.iflist]);

      // планируем запуск следующего цикла отправки hello сообщения
      this.helloTimer = setTimeout(() => this.helloLoopTick(), this.helloInterval);
    }

    this.log('verbose', 'DiscoveryClientService#helloLoopTick(): end');
  }

  /**
   * Обрабатывает принятое сообщение
   * @param {*} msg
   * @param {*} rinfo
   */
  processMessage(msg, rinfo) {
    let data;
    try {
      data = JSON.parse(msg);
    }
    catch (err) {}

    if (data && data.message === 'hello' && data.clientuid && data.clientuid === this.hostuid) {
      // это нам сообщение
      // вызываем событие 'found'
      this.emit('found', {
        hostuid: data.hostuid,
        hostname: data.hostname,
        address: rinfo.address,
      });
    }
  }

  /**
   * Создает (если надо) экземпляр сервиса
   */
  static createInstance(options, logger) {
    let instance = global[consts.DISCOVERYCLIENT_GLOBAL_NAME];

    if (!instance) {
      instance = new DiscoveryClientService(options, logger);
      global[consts.DISCOVERYCLIENT_GLOBAL_NAME] = instance;
    }
    return instance;
  }

  static getInstance() {
    return global[consts.DISCOVERYCLIENT_GLOBAL_NAME];
  }
}
